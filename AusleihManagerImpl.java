package ausleiheverwaltung.impl;

import ausleiheverwaltung.BookNotLendedException;
import ausleiheverwaltung.AusleihManager;
import ausleiheverwaltung.Ausleihe;
import ausleiheverwaltung.BookNotLendableException;
import ausleiheverwaltung.ReaderProhibitedException;
import buchverwaltung.Buch;
import buchverwaltung.BuchManager;
import java.util.ArrayList;
import java.util.List;
import leserverwaltung.Leser;
import leserverwaltung.LeserManager;
import persistent.Persistent;
import persistent.QueryNames;

/**
 *
 * @author neuhardt
 */
public class AusleihManagerImpl implements AusleihManager {

    private BuchManager buchManager;
    private LeserManager leserManager;
    private Persistent persistent;

    public void setBuchManager(BuchManager buchManager) {
        this.buchManager = buchManager;
    }

    public void setLeserManager(LeserManager leserManager) {
        this.leserManager = leserManager;
    }

    public void setPersistent(Persistent persistent) {
        this.persistent = persistent;
    }

    @Override
    public void leiheBuch(int buchId, int leserId) {
        Leser leser = leserManager.gibLeser(leserId);
        if (leser.isIstGesperrt()) {
            throw new ReaderProhibitedException();
        }
        Buch buch = buchManager.gibBuch(buchId);
        Ausleihe ausleihe = gibAusleiheFuerBuch(buchId);
        if (ausleihe != null) {
            throw new BookNotLendableException();
        }
        AusleiheImpl ausleiheImpl = new AusleiheImpl(leser, buch);
        persistent.persist(ausleiheImpl);
    }

    @Override
    public void gebeBuchZurueck(int buchId) {
        Ausleihe ausleihe = gibAusleiheFuerBuch(buchId);
        if (ausleihe == null) {
            throw new BookNotLendedException();
        }
        persistent.remove(ausleihe);
    }

    @Override
    public List<Ausleihe> gibAlleAusleihen() {
        List<AusleiheImpl> ausleihen = persistent.executeQuery(AusleiheImpl.class, QueryNames.sucheAlleAusleihen, null);
        return new ArrayList<>(ausleihen);
    }

    @Override
    public Ausleihe gibAusleiheFuerBuch(int buchId) {
        List<AusleiheImpl> ausleihen = persistent.executeQuery(AusleiheImpl.class, QueryNames.sucheAusleiheFuerBuch, buchId);
        if (ausleihen.isEmpty()) {
            return null;
        }
        return ausleihen.get(0);
    }

    @Override
    public void verlaengereAusleihe(Ausleihe ausleihe) {
        AusleiheImpl ausleiheImpl = persistent.find(AusleiheImpl.class, ausleihe.getId());
        ausleiheImpl.setVersion(ausleihe.getVersion());
        ausleiheImpl.verlaengereAusleihe();
    }

}
