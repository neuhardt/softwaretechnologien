/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package streetManagement.impl;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import streetManagement.ICrossing;
import streetManagement.ILane;
import streetManagement.IStreetInfo;
import streetManagement.Position;
import trafficsimu.SimulationConstants;
import transform.ITransform;

/**
 *
 * @author eneuhardt
 */
public class StreetManager implements IStreetInfo {

    private List<Lane> lanes;
    private List<Lane> startingLanes;
    private List<Crossing> crossings;
    private ITransform transform;

    public StreetManager(ITransform transform) {
        lanes = new ArrayList<Lane>();
        startingLanes = new ArrayList<Lane>();
        crossings = new ArrayList<Crossing>();
        crossings.add(new Crossing(SimulationConstants.widthCanvas.getValue() / 2,
                SimulationConstants.heightCanvas.getValue() / 2));
        connectCrossings();
        this.transform = transform;
    }

    public void loadStreetConfiguration(String filename) {
        lanes = new ArrayList<Lane>();
        startingLanes = new ArrayList<Lane>();
        crossings = transform.load(filename);
        connectCrossings();
    }

    // connects crossings with horizontal and vertical streets
    final public void connectCrossings() {
        List<Crossing> sortedCrossings = sortCrossings(crossings, 1);
        List<List<Crossing>> splitCrossings = splitCrossings(sortedCrossings, 1);
        for (List<Crossing> listCrossings : splitCrossings) {
            addHorizontalLanes(listCrossings);
        }
        sortedCrossings = sortCrossings(crossings, 0);
        splitCrossings = splitCrossings(sortedCrossings, 0);
        for (List<Crossing> listCrossings : splitCrossings) {
            addVerticalLanes(listCrossings);
        }
    }

    // adds vertical lanes to a list of crossings, centers of crossings must have same x-value
    final public void addVerticalLanes(List<Crossing> pCrossings) {
        Crossing crossing = pCrossings.get(0);
        Street street = new Street(new Point(crossing.getCenterX(), 0),
                new Point(crossing.getCenterX(), crossing.getNorthValueY()));
        lanes.add(street.getEntryLane(Direction.north));
        lanes.add(street.getEntryLane(Direction.south));
        startingLanes.add(street.getEntryLane(Direction.north));
        connectStreetWithCrossing(street, crossing, Direction.north);
        for (int i = 1; i < pCrossings.size(); i++) {
            street = new Street(new Point(pCrossings.get(i - 1).getCenterX(),
                    pCrossings.get(i - 1).getSouthValueY()),
                    new Point(pCrossings.get(i).getCenterX(),
                    pCrossings.get(i).getNorthValueY()));
            lanes.add(street.getEntryLane(Direction.north));
            lanes.add(street.getEntryLane(Direction.south));
            connectStreetWithCrossing(street, pCrossings.get(i - 1), Direction.south);
            connectStreetWithCrossing(street, pCrossings.get(i), Direction.north);
        }
        crossing = pCrossings.get(pCrossings.size() - 1);
        street = new Street(new Point(crossing.getCenterX(), crossing.getSouthValueY()),
                new Point(crossing.getCenterX(), SimulationConstants.heightCanvas.getValue()));
        lanes.add(street.getEntryLane(Direction.north));
        lanes.add(street.getEntryLane(Direction.south));
        connectStreetWithCrossing(street, crossing, Direction.south);
        startingLanes.add(street.getEntryLane(Direction.south));
    }

    // adds horizontal lanes to a list of crossings, centers of crossings must have same y-value
    final public void addHorizontalLanes(List<Crossing> pCrossings) {
        Crossing crossing = pCrossings.get(0);
        Street street = new Street(new Point(0, crossing.getCenterY()),
                new Point(crossing.getWestValueX(), crossing.getCenterY()));
        lanes.add(street.getEntryLane(Direction.east));
        lanes.add(street.getEntryLane(Direction.west));
        startingLanes.add(street.getEntryLane(Direction.west));
        connectStreetWithCrossing(street, crossing, Direction.west);
        for (int i = 1; i < pCrossings.size(); i++) {
            street = new Street(new Point(pCrossings.get(i - 1).getEastValueX(),
                    pCrossings.get(i - 1).getCenterY()),
                    new Point(pCrossings.get(i).getWestValueX(),
                    pCrossings.get(i).getCenterY()));
            lanes.add(street.getEntryLane(Direction.east));
            lanes.add(street.getEntryLane(Direction.west));
            connectStreetWithCrossing(street, pCrossings.get(i - 1), Direction.east);
            connectStreetWithCrossing(street, pCrossings.get(i), Direction.west);
        }
        crossing = pCrossings.get(pCrossings.size() - 1);
        street = new Street(new Point(crossing.getEastValueX(), crossing.getCenterY()),
                new Point(SimulationConstants.widthCanvas.getValue(), crossing.getCenterY()));
        lanes.add(street.getEntryLane(Direction.east));
        lanes.add(street.getEntryLane(Direction.west));
        connectStreetWithCrossing(street, crossing, Direction.east);
        startingLanes.add(street.getEntryLane(Direction.east));
    }

    // adds end of a street with a crossing, direction defines the end of the crossing
    public void connectStreetWithCrossing(Street street, Crossing crossing, Direction direction) {
        street.getEntryLane(direction).addNextLanes(crossing.getEntryLanes(direction));
        for (Lane lane : crossing.getExitLanes(direction)) {
            lane.addNextLane(street.getEntryLane(Direction.getDirection((direction.getValue() + 2) % 4)));
        }
    }
    
    // inserts a crossing into a sorted list, sortOrder determines whether x or y value is taken first
    public void insertCrossing(List<Crossing> pCrossings, Crossing pCrossing, int sortOrder) {
        int i = 0;
        while (i < pCrossings.size()) {
            if (pCrossing.isBefore(pCrossings.get(i), sortOrder)) {
                break;
            }
            i++;
        }
        pCrossings.add(i, pCrossing);
    }

    // sorts crossings according to sortorder (0 = take x value first, 1 = take y value first)
    public List<Crossing> sortCrossings(List<Crossing> pCrossings, int sortOrder) {
        List<Crossing> sortedCrossings = new ArrayList<Crossing>();
        for (Crossing crossing : pCrossings) {
            insertCrossing(sortedCrossings, crossing, sortOrder);
        }
        return sortedCrossings;
    }
    
    // splits sorted crossings into lists withe same x- or y- value
    public List<List<Crossing>> splitCrossings(List<Crossing> pCrossings, int sortOrder) {
        List<List<Crossing>> splitCrossings = new ArrayList<List<Crossing>>();
        List<Crossing> element = new ArrayList<Crossing>();
        element.add(pCrossings.get(0));
        splitCrossings.add(element);
        for (int i = 1; i < pCrossings.size(); i++) {
            if (sortOrder == 0 && pCrossings.get(i).getCenter().x != element.get(0).getCenter().x) {
                element = new ArrayList<Crossing>();
                splitCrossings.add(element);
            }
            if (sortOrder == 1 && pCrossings.get(i).getCenterY() != element.get(0).getCenterY()) {
                element = new ArrayList<Crossing>();
                splitCrossings.add(element);
            }
            element.add(pCrossings.get(i));
        }
        return splitCrossings;
    }

    public List<ILane> getLanes() {
        return new ArrayList(lanes);
    }

    public List<ILane> getStartingLanes() {
        return new ArrayList(startingLanes);
    }

    public List<ICrossing> getCrossings() {
        return new ArrayList<ICrossing>(crossings);
    }

    public Point getAbsolutePosition(Position position) {
        Lane lane = findLane(position.getLane().getStart(), position.getLane().getEnd());
        return lane.getPoint(position.getPos());
    }

    // returns the lane with start and end point
    public Lane findLane(Point start, Point end) {
        for (Lane lane : lanes) {
            if (lane.getStart().equals(start) && lane.getEnd().equals(end)) {
                return lane;
            }
        }
        for (Crossing crossing : crossings) {
            for (Lane lane : crossing.getLanes()) {
                if (lane.getStart().equals(start) && lane.getEnd().equals(end)) {
                    return lane;
                }
            }
        }
        return null;
    }

    public boolean posNearEndOfLane(ILane lane, int pos, int speed) {
        return ((Lane)lane).posNearEndOfLane(pos, speed);
    }

    public List<ILane> getNextLanes(ILane lane) {
        return new ArrayList<ILane>(((Lane)lane).getNextLanes());
    }

    public ILane getNextLaneAfterCrossing(ILane lane, int target) {
        return ((Lane)lane).getCrossing().getNextLaneAfterCrossing((Lane)lane);
    }
}
